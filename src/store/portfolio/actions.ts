import { ActionTree } from 'vuex';
import { RootState } from '../../entity'
import { Asset } from '../../entity'
import { PortfolioState } from '../../entity'

export const actions: ActionTree<PortfolioState, RootState> = {
	getAssets: (context, ID: string): any => {
        let assets = Array<Asset>();
		fetch(`http://0.0.0.0:8000/portfolio/${ID}`)
            .then(response => response.json())
            .then(fetchedAssets => {
				context.commit('setAssets', fetchedAssets);
			})
			.catch(e => console.log(e.message));
	},
	getCombos: (context, amount: number): any => {
		fetch(`http://0.0.0.0:8000/portfolio/dry/${amount}`)
            .then(response => response.json())
            .then(fetchedCombos => {
				context.commit('setCombos', fetchedCombos);
			})
			.catch(e => console.log(e.message));
	}
};
