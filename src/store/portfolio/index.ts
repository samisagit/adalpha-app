import { Module } from 'vuex';
import { getters } from './getters';
import { actions } from './actions';
import { mutations } from './mutations';
import { PortfolioState } from '../../entity';
import { RootState } from '../../entity';
import { Asset } from '../../entity';

export const state: PortfolioState = {
    assets: Array<Asset>(),
    combos: Array<Array<Asset>>(),
    balance: 0
};

const namespaced: boolean = true;

export const portfolio: Module<PortfolioState, RootState> = {
    namespaced,
    state,
    getters,
    actions,
    mutations
};