import { Asset } from '../../entity'

export const mutations = {
	setAssets(state:any, content: Array<Asset>) {
		state.assets = content
	},
	setCombos(state:any, content: Array<Array<Asset>>) {
		state.combos = content
	}
}
