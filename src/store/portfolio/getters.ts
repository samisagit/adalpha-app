import { PortfolioState } from '../../entity'
import { Asset } from '../../entity'

export const getters = {
	assets: (state:PortfolioState) :Array<Asset> => {
		return state.assets
	},
	combos: (state:PortfolioState) :Array<Array<Asset>> => {
		return state.combos
	}
}
