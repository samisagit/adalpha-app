import Vue from "vue";
import Vuex from "vuex"
import VueRouter from "vue-router"
import Portfolio from "./layouts/portfolio.vue";
import Combos from "./layouts/combos.vue";
import Router from "./layouts/router.vue";
import store from "./store/index"

Vue.use(Vuex)
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'home',
        component: Portfolio
    },
    {
        path: '/combos',
        name: 'combos',
        component: Combos
    }
]

const router = new VueRouter({
    routes
})

let v = new Vue({
    template: `
        <div>
            <router-view></router-view>
        </div>
  `,
    store,
    router,

}).$mount('#app')
