export interface RootState {
    version: string;
}

export interface Asset {
    value: number;
    ISIN: string;
    name: string;
}

export interface PortfolioState {
    assets: Array<Asset>;
    combos: Array<Array<Asset>>;
    balance: number;
}